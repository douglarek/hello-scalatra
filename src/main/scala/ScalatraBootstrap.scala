import javax.servlet.ServletContext
import me.icocoa.app._
import org.scalatra._
import org.slf4j.LoggerFactory
import slick.jdbc.JdbcBackend.Database

class ScalatraBootstrap extends LifeCycle {

  val logger = LoggerFactory.getLogger(getClass())

  val cpds = new com.mchange.v2.c3p0.ComboPooledDataSource

  logger.info("Created c3p0 connection pool ...")

  override def init(context: ServletContext) {

    val db = Database.forDataSource(cpds)
    context.mount(new HelloScalatraServlet(db), "/*")
  }

  private def closeDbConnection() {
    logger.info("Closing c3po connection pool")
    cpds.close()
  }

  override def destroy(context: ServletContext) {
    super.destroy(context)
    closeDbConnection()
  }
}
