# Hello Scalatra#

## Build & Run ##

```sh
$ sbt
> container:start
> browse

$ sbt assembly

$ java -jar target/scala-2.11/hello-scalatra-assembly-0.1.0-SNAPSHOT.jar
```

If `browse` doesn't launch your browser, manually open [http://localhost:8080/](http://localhost:8080/) in your browser.
